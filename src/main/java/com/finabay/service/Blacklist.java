package com.finabay.service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

//TODO change it. Mb make file config
public class Blacklist {

    private  static Set<String> blacklistedIds = new HashSet<>();
    static {
        blacklistedIds.add("1");
        blacklistedIds.add("3");
        blacklistedIds.add("5");
        blacklistedIds.add("7");
        blacklistedIds.add("9");
        blacklistedIds.add("11");
        blacklistedIds.add("13");
    }


    static boolean contains(String id){
        return blacklistedIds.contains(id);
    }
}
