package com.finabay.service;

import com.finabay.service.model.LoanInfo;
import com.finabay.service.model.UserInfo;
import com.finabay.storage.LoanStorage;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

public class LoanServiceImpl implements LoanService {

    private static final long  LOAN_TIMEFRAME = 15 * 60 * 1000L;    //15 minutes

    private LoanStorage storage;

    public LoanServiceImpl(LoanStorage storage) {
        this.storage = storage;
    }

    @Override
    public boolean applyForLoan(UserInfo userInfo, LoanInfo loanInfo) {
        //TODO check that data not null and valid
        if(userInfo != null && loanInfo != null) {
            if (!inBlackList(userInfo.getPersonalId()) && isCountryValid(loanInfo.getCountryCode())) {
                storage.saveLoan(userInfo.getPersonalId(), loanInfo);
                return true;
            }
        }
        return false;
    }


    private boolean inBlackList(String personalId) {
        return Blacklist.contains(personalId);
    }

    private boolean isCountryValid(String countryCode) {
        Long time = storage.lastLoanTimeForCounty(countryCode);
        return !(time != null && time + LOAN_TIMEFRAME > System.currentTimeMillis());
    }
}
