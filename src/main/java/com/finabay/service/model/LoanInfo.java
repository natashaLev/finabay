package com.finabay.service.model;

import java.math.BigDecimal;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

public class LoanInfo {

    private BigDecimal amount;
    private long term;
    private long wasGiven;
    private String countryCode;
    private String personalId;

    public LoanInfo(BigDecimal amount, long term, long wasGiven, String countryCode, String personalId) {
        this.amount = amount;
        this.term = term;
        this.wasGiven = wasGiven;
        this.countryCode = countryCode;
        this.personalId = personalId;
    }

    public LoanInfo() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getTerm() {
        return term;
    }

    public void setTerm(long term) {
        this.term = term;
    }

    public long getWasGiven() {
        return wasGiven;
    }

    public void setWasGiven(long wasGiven) {
        this.wasGiven = wasGiven;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}

