package com.finabay.service.model;

import java.util.List;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

public class UserInfo {

    private String name;
    private String surname;
    private String personalId;
    private List<LoanInfo> loanIfos;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public UserInfo(String name, String surname, String personalId) {

        this.name = name;
        this.surname = surname;
        this.personalId = personalId;
    }

    public void setLoans(List<LoanInfo> loanInfos) {
        this.loanIfos = loanInfos;
    }
}
