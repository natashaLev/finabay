package com.finabay.service;

import com.finabay.service.model.LoanInfo;
import com.finabay.service.model.UserInfo;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

public interface LoanService {

    boolean applyForLoan(UserInfo userInfo, LoanInfo loanInfo);

}
