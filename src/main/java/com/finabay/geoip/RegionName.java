package com.finabay.geoip;

public class RegionName {
  static public String regionNameByCode(String country_code,String region_code) {
    String name = null;
    int region_code2 = -1;
    if (region_code == null) { return null; }
    if (region_code.equals("")) { return null; }

    if (    ((region_code.charAt(0) >= 48 ) && ( region_code.charAt(0) < ( 48 + 10 )))
            && ((region_code.charAt(1) >= 48 ) && ( region_code.charAt(1) < ( 48 + 10 )))
            ){
        // only numbers, that shorten the large switch statements
        region_code2 = (region_code.charAt(0)- 48) * 10 + region_code.charAt(1) - 48;
    }
    else if (    (    ((region_code.charAt(0) >= 65) && (region_code.charAt(0) < (65 + 26)))
            || ((region_code.charAt(0) >= 48) && (region_code.charAt(0) < (48 + 10))))
            && (    ((region_code.charAt(1) >= 65) && (region_code.charAt(1) < (65 + 26)))
            || ((region_code.charAt(1) >= 48) && (region_code.charAt(1) < (48 + 10))))
            ) {

        region_code2 = (region_code.charAt(0) - 48) * (65 + 26 - 48) + region_code.charAt(1) - 48 + 100;
    }

    if (region_code2 == -1) {return null;}
    if (country_code.equals("CA") == true) {
        switch (region_code2) {

            case 849:
                name = "Alberta";
                break;
            case 893:
                name = "British Columbia";
                break;
            case 1365:
                name = "Manitoba";
                break;
            case 1408:
                name = "New Brunswick";
                break;
            case 1418:
                name = "Newfoundland";
                break;
            case 1425:
                name = "Nova Scotia";
                break;
            case 1427:
                name = "Nunavut";
                break;
            case 1463:
                name = "Ontario";
                break;
            case 1497:
                name = "Prince Edward Island";
                break;
            case 1538:
                name = "Quebec";
                break;
            case 1632:
                name = "Saskatchewan";
                break;
            case 1426:
                name = "Northwest Territories";
                break;
            case 1899:
                name = "Yukon Territory";
                break;
        }
    }
    if (country_code.equals("LV") == true) {
        switch (region_code2) {
            case 1:
                name = "Aizkraukles";
                break;
            case 2:
                name = "Aluksnes";
                break;
            case 3:
                name = "Balvu";
                break;
            case 4:
                name = "Bauskas";
                break;
            case 5:
                name = "Cesu";
                break;
            case 6:
                name = "Daugavpils";
                break;
            case 7:
                name = "Daugavpils";
                break;
            case 8:
                name = "Dobeles";
                break;
            case 9:
                name = "Gulbenes";
                break;
            case 10:
                name = "Jekabpils";
                break;
            case 11:
                name = "Jelgava";
                break;
            case 12:
                name = "Jelgavas";
                break;
            case 13:
                name = "Jurmala";
                break;
            case 14:
                name = "Kraslavas";
                break;
            case 15:
                name = "Kuldigas";
                break;
            case 16:
                name = "Liepaja";
                break;
            case 17:
                name = "Liepajas";
                break;
            case 18:
                name = "Limbazu";
                break;
            case 19:
                name = "Ludzas";
                break;
            case 20:
                name = "Madonas";
                break;
            case 21:
                name = "Ogres";
                break;
            case 22:
                name = "Preilu";
                break;
            case 23:
                name = "Rezekne";
                break;
            case 24:
                name = "Rezeknes";
                break;
            case 25:
                name = "Riga";
                break;
            case 26:
                name = "Rigas";
                break;
            case 27:
                name = "Saldus";
                break;
            case 28:
                name = "Talsu";
                break;
            case 29:
                name = "Tukuma";
                break;
            case 30:
                name = "Valkas";
                break;
            case 31:
                name = "Valmieras";
                break;
            case 32:
                name = "Ventspils";
                break;
            case 33:
                name = "Ventspils";
                break;
        }
    }
    if (country_code.equals("BY") == true) {
        switch (region_code2) {
            case 1:
                name = "Brestskaya Voblasts'";
                break;
            case 2:
                name = "Homyel'skaya Voblasts'";
                break;
            case 3:
                name = "Hrodzyenskaya Voblasts'";
                break;
            case 4:
                name = "Minsk";
                break;
            case 5:
                name = "Minskaya Voblasts'";
                break;
            case 6:
                name = "Mahilyowskaya Voblasts'";
                break;
            case 7:
                name = "Vitsyebskaya Voblasts'";
                break;
        }
    }
    return name;
  }
}
