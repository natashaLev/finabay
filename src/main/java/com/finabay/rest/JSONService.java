package com.finabay.rest;

import com.finabay.location.GetLocation;
import com.finabay.service.LoanService;
import com.finabay.service.model.LoanInfo;
import com.finabay.service.model.UserInfo;
import com.finabay.storage.LoanStorage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Natasha Levkovich
 * Date: 29.08.15
 */

@Path("/json/finabay")
public class JSONService {

    private LoanStorage loanStorage;

    private LoanService loanService;

    @GET
    @Path("/getAllLoans")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LoanInfo> getAllLoansJSON() {
        //testing data
        GetLocation obj = new GetLocation();

        List<LoanInfo> result = new ArrayList<>();
        LoanInfo loanInfo = new LoanInfo();
        loanInfo.setCountryCode(obj.getLocation("206.190.36.45").toString());
        loanInfo.setPersonalId("1");
        loanInfo.setAmount(BigDecimal.valueOf(5000));
        loanInfo.setTerm(5);

        LoanInfo loanInfo1 = new LoanInfo();
        loanInfo1.setCountryCode(obj.getLocation("206.190.36.45").toString());
        loanInfo1.setPersonalId("2");
        loanInfo1.setAmount(BigDecimal.valueOf(5000));
        loanInfo1.setTerm(5);

        result.add(loanInfo);
        result.add(loanInfo1);


        return result;
       // return loanStorage.getAllLoans();

    }

    @POST
	@Path("/post")
    @Produces(MediaType.APPLICATION_JSON)
    public Response applyLoanInJSON(UserInfo user, LoanInfo loan) {
        try {
            if (loanService.applyForLoan(user, loan)) {
                String result = "Track saved : " + loan + user;
                return Response.status(201).entity(result).build();
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return null;
    }


}