package com.finabay.storage.impl;
import com.finabay.service.model.LoanInfo;
import com.finabay.storage.LoanStorage;

import java.util.*;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 */

// We can switch it for DB storage and etc.
public class LoanStorageImpl implements LoanStorage {

    private Map<String, List<LoanInfo>> loans = new HashMap<>();
    private Map<String, Long> countries = new HashMap<>();


    @Override
    public boolean saveLoan(String userId, LoanInfo loan) {
        List<LoanInfo> userLoans = getLoansByUserId(userId);
        if (userLoans == null) {
            userLoans = new ArrayList<>();
        }
        userLoans.add(loan);
        loans.put(userId, userLoans);
        saveTime(loan.getCountryCode());
        return true;
    }

    private void saveTime(String countryCode){
       countries.put(countryCode, System.currentTimeMillis());
    }

    @Override
    public List<LoanInfo> getLoansByUserId(String userId) {
        return loans.get(userId);
    }

    @Override
    public List<LoanInfo> getAllLoans() {
        List<LoanInfo> result = new ArrayList<>(loans.values().size());   // we need more
        for (List<LoanInfo> userLoans : loans.values()) {
            result.addAll(userLoans);
        }
        return result;
    }

    @Override
    public Long lastLoanTimeForCounty(String countryCode) {
        return countries.get(countryCode);
    }
}
