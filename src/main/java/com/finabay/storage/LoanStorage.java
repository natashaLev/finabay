package com.finabay.storage;

import com.finabay.location.ServerLocation;
import com.finabay.service.model.LoanInfo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * @author Natasha Levkovich
 *         Date: 22.08.15
 */

public interface LoanStorage {

    //TODO replace it with models fot storage level
    boolean saveLoan(String userId, LoanInfo loan);

    @Nullable
    List<LoanInfo> getLoansByUserId(String userId);

    @Nonnull
    List<LoanInfo> getAllLoans();

    @Nullable
    Long lastLoanTimeForCounty(String countryCode);


}
