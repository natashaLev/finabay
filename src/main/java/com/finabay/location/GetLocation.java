package com.finabay.location;

import com.finabay.geoip.Location;
import com.finabay.geoip.LookupService;

import java.io.File;
import java.io.IOException;

//import com.sun.corba.se.spi.activation.LocatorPackage.ServerLocation;



public class GetLocation {

    public static void main(String[] args) {
        GetLocation obj = new GetLocation();
        ServerLocation location = obj.getLocation("206.190.36.45");
        System.out.println(location);
    }

    public ServerLocation getLocation(String ipAddress) {

        File file = new File(
                "\\GeoLiteCity.dat");
        return getLocation(ipAddress, file);

    }

    public ServerLocation getLocation(String ipAddress, File file) {

        ServerLocation serverLocation = null;

        try {

            serverLocation = new ServerLocation();

            LookupService lookup = new LookupService(file,LookupService.GEOIP_MEMORY_CACHE);
            Location locationServices = lookup.getLocation(ipAddress);

            serverLocation.setCountryCode(locationServices.countryCode);
            serverLocation.setCountryName(locationServices.countryName);
            serverLocation.setCity(locationServices.city);

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return serverLocation;

    }
}
