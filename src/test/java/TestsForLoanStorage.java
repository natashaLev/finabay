import com.finabay.service.model.LoanInfo;
import com.finabay.service.model.UserInfo;
import com.finabay.storage.LoanStorage;
import com.finabay.storage.impl.LoanStorageImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 *
 * Тест на проверку методов класса LoanStorage
 */

public class TestsForLoanStorage {

    private LoanStorage loanStorage;

    //перед каждым тестом содаем экземпляр
    @Before
    public void executedBeforeEach()
    {
        loanStorage = new LoanStorageImpl();
    }

    //достанем все кредиты чувака
    @Test
    public void shouldFindCustomerLoans() throws Exception
    {
        UserInfo client = FakeData.simpleUser();
        LoanInfo loanInfo = FakeData.simpleLoan(BigDecimal.TEN, client);
        loanStorage.saveLoan(FakeData.PERSON_ID, loanInfo);
        client.setLoans(Arrays.asList(loanInfo));
        List<LoanInfo> loans = loanStorage.getLoansByUserId(FakeData.PERSON_ID);

        assert (loans.size() == 1);

    }

    //достанем все кредиты
    @Test
    public void testFindAllLoans() throws Exception
    {
        UserInfo client = FakeData.simpleUser();
        UserInfo clientTwo = FakeData.simpleUserTwo();
        LoanInfo loanInfo = FakeData.simpleLoan(BigDecimal.TEN, client);
        loanStorage.saveLoan(FakeData.PERSON_ID, loanInfo);
        client.setLoans(Arrays.asList(loanInfo));
        LoanInfo loanInfoTwo = FakeData.simpleLoan(BigDecimal.TEN, clientTwo);
        loanStorage.saveLoan(FakeData.PERSON_ID_TWO, loanInfoTwo);
        clientTwo.setLoans(Arrays.asList(loanInfoTwo));

        List<LoanInfo> foundLoans = loanStorage.getAllLoans();

        assert (foundLoans.size() == 2);
    }
}
