import com.finabay.service.LoanService;
import com.finabay.service.LoanServiceImpl;
import com.finabay.service.model.LoanInfo;
import com.finabay.service.model.UserInfo;
import com.finabay.storage.LoanStorage;
import com.finabay.storage.impl.LoanStorageImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNull;

/**
 * @author Natasha Levkovich
 * Date: 28.08.15
 *
 * Тест на проверку методов класса LoanService(проверка логики)
 */

public class TestsForLoanService {

    private LoanService loanService;

    private LoanStorage loanStorage;

    //перед каждым тестом содаем экземпляр
    @Before
    public void executedBeforeEach()
    {
        loanStorage = new LoanStorageImpl();
        loanService = new LoanServiceImpl(loanStorage);
    }

    //примем ли заявку на заем
    @Test
    public void testForStrategyLoan() throws Exception
    {
        UserInfo client = FakeData.simpleUser();
        LoanInfo loanInfo = FakeData.simpleLoan(BigDecimal.TEN, client);
        client.setLoans(Arrays.asList(loanInfo));
        loanService.applyForLoan(client, loanInfo);
        //loanService.applyForLoan(client, FakeData.simpleLoan(BigDecimal.TEN, client));

        assert (true);

    }

    //отказ в заявке на заем, тк countryCode равен у клиентов и по таймеру не закончилось время
    @Test
    public void testForStrategyLoanWhenResultDenied() throws Exception
    {
        UserInfo client = FakeData.simpleUser();
        LoanInfo loanInfo = FakeData.simpleLoan(BigDecimal.TEN, client);
        loanService.applyForLoan(client, loanInfo);
        UserInfo clientTwo = FakeData.simpleUserTwo();
        LoanInfo loanInfoTwo = FakeData.simpleLoan(BigDecimal.TEN, clientTwo);
        loanService.applyForLoan(clientTwo, loanInfoTwo);
        List<LoanInfo> foundLoans = loanStorage.getAllLoans();

        assert (foundLoans.size() == 1);//должно быть 1

    }

    //отказ в заявке на заем, тк чувак из blackList-а
    @Test
    public void testForStrategyLoanWhenUsedAccountFromBlacklist() throws Exception
    {
        UserInfo client = FakeData.simpleUserFromBlackList();
        LoanInfo loanInfo = FakeData.simpleLoan(BigDecimal.TEN, client);
        client.setLoans(Arrays.asList(loanInfo));
        loanService.applyForLoan(client, loanInfo);
        List<LoanInfo> loans = loanStorage.getLoansByUserId(FakeData.PERSON_ID_FROM_BLACKLIST);

        //Check that an list is null
        assertNull (loans);

    }
}
