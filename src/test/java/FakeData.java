import com.finabay.service.model.UserInfo;
import com.finabay.service.model.LoanInfo;

import java.math.BigDecimal;

/**
 * @author Natasha Levkovich
 * Date: 22.08.15
 *
 * Фейковые данные
 */
public class FakeData {

    public static final String FIRST_NAME = "Michael";

    public static final String LAST_NAME = "Jordan";

    public static final String PERSON_ID = "17";

    public static final String PERSON_ID_FROM_BLACKLIST = "13";

    public static final String PERSON_ID_TWO = "18";

    public static final String IP = "195.122.30.0";

    public static final Long TERM = 5L;


    public static UserInfo simpleUser()
    {
        return new UserInfo(FIRST_NAME, LAST_NAME, PERSON_ID);
    }


    public static UserInfo simpleUserTwo()
    {
        return new UserInfo(FIRST_NAME, LAST_NAME, PERSON_ID_TWO);
    }

    public static UserInfo simpleUserFromBlackList()
    {
        return new UserInfo(FIRST_NAME, LAST_NAME, PERSON_ID_FROM_BLACKLIST);
    }


    public static LoanInfo simpleLoan(BigDecimal amount, UserInfo userInfo)
    {
        LoanInfo loan = new LoanInfo();
        loan.setAmount(amount);
        loan.setCountryCode(IP);
        loan.setTerm(TERM);
        loan.setPersonalId(userInfo.getPersonalId());
        return loan;
    }
}
